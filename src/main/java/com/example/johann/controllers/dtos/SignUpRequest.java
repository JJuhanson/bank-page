package com.example.johann.controllers.dtos;

import javax.validation.constraints.NotNull;

public class SignUpRequest {

	@NotNull(message = "Please provide username")
	private String username;

	@NotNull(message = "Please provide password")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
