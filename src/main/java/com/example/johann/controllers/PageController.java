package com.example.johann.controllers;

import com.example.johann.Constants;
import com.example.johann.services.AdminPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

    @Autowired
    private AdminPageService adminPageService;


    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model){ return "login";}

    @GetMapping("/signup")
    public String signup(Model model){ return "signup";}

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String admin(Model model) {
        model.addAttribute("users", adminPageService.getAllNotAdminUsers());
        return "admin";
    }

    @GetMapping("/exchange")
    @PreAuthorize("hasRole('ROLE_" + Constants.ACCESS_RIGHT_USER + "')")
    public String exchange(Model model) {
        return "exchange";
    }


}
