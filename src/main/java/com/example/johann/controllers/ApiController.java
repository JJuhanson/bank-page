package com.example.johann.controllers;

import com.example.johann.ClientException;
import com.example.johann.Constants;
import com.example.johann.controllers.dtos.SignUpRequest;
import com.example.johann.services.SignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@RestController
public class ApiController {

	@Autowired
	private SignUpService signUpService;

	@GetMapping(value = "/api/exchange", produces = "application/json")
	@PreAuthorize("hasRole('ROLE_" + Constants.ACCESS_RIGHT_USER + "')")
	public Object exchangeCurrency() {
		return Collections.singletonMap("success", true);
	}

	@PostMapping(value = "/api/signup", produces = "application/json")
	public Map<String, Object> signup(@RequestBody @Valid SignUpRequest dto) {
		signUpService.signUpUser(dto);
		return Collections.singletonMap("success", true);
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ClientException.class)
	public Map<String, String> handleClientError(ClientException ex) {
		return Collections.singletonMap("message", ex.getMessage());
	}

}
