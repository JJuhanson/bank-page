package com.example.johann.auth;

import com.example.johann.database.users.User;
import com.example.johann.database.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalService implements UserDetailsService {

	private UserRepository userRepository;

	public UserPrincipalService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsernameAndArchived(username, false);
		if (user == null) {
			throw new UsernameNotFoundException("Could not find username: " + username);
		}
		return new UserPrincipal(user);
	}
}
