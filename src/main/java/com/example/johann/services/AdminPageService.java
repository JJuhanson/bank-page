package com.example.johann.services;

import com.example.johann.Constants;
import com.example.johann.database.users.User;
import com.example.johann.database.users.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminPageService {

	private UserRepository repository;

	public AdminPageService(UserRepository repository) {
		this.repository = repository;
	}

	public List<User> getAllNotAdminUsers() {
		return repository.findByAccess(Constants.ACCESS_RIGHT_USER);
	}


}
