package com.example.johann.services;

import com.example.johann.ClientException;
import com.example.johann.controllers.dtos.SignUpRequest;
import com.example.johann.database.users.User;
import com.example.johann.database.users.UserRepository;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SignUpService {

	private UserRepository repository;
	private PasswordEncoder passwordEncoder;

	public SignUpService(UserRepository repository, PasswordEncoder passwordEncoder) {
		this.repository = repository;
		this.passwordEncoder = passwordEncoder;
	}

	@Nullable
	public void signUpUser(SignUpRequest dto) {
		User currentlySignedUp = repository.findByUsernameAndArchived(dto.getUsername(), false);
		if (currentlySignedUp != null) {
			throw new ClientException("Username is already taken");
		}
		User user = new User(dto.getUsername(), passwordEncoder.encode(dto.getPassword()));
		repository.save(user);
	}
}
