package com.example.johann.database.users;

import com.example.johann.Constants;

import javax.persistence.*;

@Entity
@Table(name = "USER")
public class User {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "ACCESS")
	private String access;

	@Column(name = "ARCHIVED")
	private boolean archived;

	public User(){

	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
		this.archived = false;
		this.access = Constants.ACCESS_RIGHT_USER;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}
}
