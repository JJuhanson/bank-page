package com.example.johann.database.users;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsernameAndArchived(String username, boolean archived);
	List<User> findByAccess(String access);
}
